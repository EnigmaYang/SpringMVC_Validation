<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	this is for login Form
	<br>
	<form:errors path="userForm.*" cssStyle="color:blue" />
	<form:form modelAttribute="userForm" action="login" method="post">
		username:<input type="text" name="username">
		<form:errors path="username" cssStyle="color:red" />
		<br>
		passowrd:<input type="password" name="password">
		<form:errors path="password" cssStyle="color:red" />
		<br>
		email:<input type="text" name="email">
		<form:errors path="email" cssStyle="color:red" />
		<br>
		age:<input type="text" name="age">
		<form:errors path="age" cssStyle="color:red" />
		<br>
		date of birth:<input type="text" name="dob">
		<form:errors path="dob" cssStyle="color:red" />
		<br>
		<input type="submit">
	</form:form>
</body>
</html>