package cn.yawei;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {
	@RequestMapping("/loginUI")
	public String testLogin() {
		return "login";
	}

	@RequestMapping("/login")
	public String testSubmit(@Valid @ModelAttribute("userForm") User user, BindingResult result) {

		if (result.hasErrors()) {
			// return "redirect:fillform";
			return "login";
		}
		System.out.println("username\t" + user.getUsername());
		System.out.println("password\t" + user.getPassword());
		System.out.println("email   \t" + user.getEmail());
		System.out.println("age     \t" + user.getAge());
		System.out.println("dob     \t" + user.getDob());

		return "success";
	}
}
