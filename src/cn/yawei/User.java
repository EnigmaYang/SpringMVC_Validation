package cn.yawei;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
//import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

public class User {
	// @NotEmpty
	@NotBlank(message = "username cannot be empty.")
	private String username;

	@Size(min = 4, message = "password should more than 3 characters.")
	private String password;

	@Email(message = "invalid email format.")
	private String email;

	@NotNull(message = "you must put some value for age")
	@Max(value = 200, message = "age should be smaller then {1}")
	// in this, {1} is get the value form first parameter in Annotation. {0} use
	// to get the original data you input
	@Min(value = 0, message = "age should be greater than {1}")
	private int age;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	// default error message is not in message and we need use
	// message.properties file load the error message
	private Date dob;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

}
